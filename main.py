import re
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import unidecode

DATASET_PATH = "data/sanepar_dados_brutos.xlsx"
VARIABLES = ["freq", "ft", "pt"]
AXIS_LABELS = {
    "timestamp": "Data",
    "freq_b1": "Frequência de operação da bomba 1 (B1)",
    "freq_b2": "Frequência de operação da bomba 2 (B2)",
    "freq_b3": "Frequência de operação da bomba 3 (B3)",
    "ft01_ent_irai": "Vazão de entrada (FT01)",
    "ft02_gbal": "Vazão de gravidade (FT02)",
    "ft03_rbal": "Vazão de recalque (FT03)",
    "pt01_succao": "Pressão de sucção (PT01)",
    "pt02_rbal": "Pressão de recalque (PT02)",
    "lt01_nivel": "Nível do Reservatório (LT01)"
}


class Visualization:
    def __init__(self):
        pass

    def _violin_plot(self,
                     data: pd.DataFrame,
                     column_name: str,
                     level: str,
                     ax: plt.Axes,
                     quantiles: List[float] = None,
                     title: str = ""):

        column = data[column_name]
        unstacked = column.unstack(level=level)
        columns = self._get_columns(unstacked)

        ax.violinplot(columns, showextrema=False)

        index = np.arange(1, len(columns)+1)
        if not quantiles is None:
            for quan in quantiles:
                percentiles = [np.percentile(col, quan) for col in columns]
                ax.plot(index, percentiles, label=f"Percentil {quan}")

        ax.set_title(title)
        ax.legend()

    def _plot_band(self,
                   data: pd.DataFrame,
                   column_name,
                   ax: plt.Axes,
                   period="1d",
                   color=None,
                   title: str = ""):

        mean_df = data.resample(period).mean()
        std_df = data.resample(period).std()

        ax.plot(mean_df.index, mean_df[column_name], color=color)
        ax.fill_between(mean_df.index,
                        mean_df[column_name] - std_df[column_name],
                        mean_df[column_name] + std_df[column_name],
                        alpha=0.2,
                        color=color)

        ax.set_title(title)

    @staticmethod
    def _get_columns(df):
        cols = []
        for col in df.columns:
            column = df[col].dropna()
            cols.append(column)

        return cols


class EDA:
    _visualization = Visualization()

    def __init__(self, dataset_path: str, variables: List[str], resample_period: str = None, labels: str = None):
        self._variables = variables
        self._quantiles = [25, 50, 75]
        self._years = [2018, 2019]

        self._read_file(dataset_path, resample_period)

        if labels is None:
            labels = {x: x for x in self._data.columns.to_list()}

        self._labels = labels

        self._plot_data()

    def _read_file(self, dataset_path: str, resample_period: str = None):
        df = pd.read_excel(dataset_path)

        cols = [self._simplify_column_name(col) for col in df.columns]
        df.columns = cols

        df.set_index(keys="timestamp", inplace=True)
        df.replace(to_replace="Bad", value=np.nan, inplace=True)
        df.interpolate(inplace=True)
        df = df.groupby("timestamp").mean()

        self._data = df

        if not resample_period is None:
            self._resampled = self._data.resample(resample_period).mean()
        else:
            self._resampled = self._data

        multiindex_df = df.copy()
        multiindex_df["month"] = multiindex_df.index.month
        multiindex_df["dayofweek"] = multiindex_df.index.dayofweek
        multiindex_df["hour"] = multiindex_df.index.hour
        multiindex_df.set_index(
            ["dayofweek", "month", "hour"], append=True, inplace=True)

        self._multiindex_data = multiindex_df

    def _plot_data(self):
        for var in self._variables:
            columns = [col for col in self._data.columns.to_list()
                       if col.startswith(var)]

            fig1, ax = plt.subplots(
                len(columns), 1, sharex=True, figsize=(10, 10))
            fig2, bx = plt.subplots(
                len(columns), 1, sharex=True, figsize=(10, 10))
            fig3, cx = plt.subplots(
                len(columns), 1, sharex=True, figsize=(10, 10))
            fig4, dx = plt.subplots(
                len(columns), 1, sharex=True, figsize=(10, 10))
            fig5, fx = plt.subplots(
                len(columns), 1, sharex=True, figsize=(10, 10))
            fig6, gx = plt.subplots(
                len(columns), 1, sharex=True, figsize=(10, 10))

            fig2.suptitle(
                "Distribuição em relação aos meses do ano", fontsize=16)
            fig3.suptitle(
                "Distribuição em relação aos dias da semana", fontsize=16)
            fig4.suptitle(
                "Distribuição em relação aos horários do dia", fontsize=16)

            ax = ax.flatten()
            bx = bx.flatten()
            cx = cx.flatten()
            dx = dx.flatten()
            fx = fx.flatten()
            gx = gx.flatten()

            for i, col in enumerate(columns):
                self._visualization._plot_band(
                    self._data, col, ax[i], period="1d", title=self._labels[col])
                self._visualization._violin_plot(
                    self._multiindex_data, col, "month", bx[i], quantiles=self._quantiles, title=self._labels[col])
                self._visualization._violin_plot(
                    self._multiindex_data, col, "dayofweek", cx[i], quantiles=self._quantiles, title=self._labels[col])
                self._visualization._violin_plot(
                    self._multiindex_data, col, "hour", dx[i], quantiles=self._quantiles, title=self._labels[col])

                data = self._resampled if self._resampled is not None else self._data

                bins = 20 if col == "pt01_succao" else 100
                sns.histplot(data=data, x=col, kde=True, bins=bins, ax=fx[i])
                fx[i].set_title(self._labels[col])

                for year in self._years:
                    filtered_data = data[data.index.year == year]
                    gx[i].plot(filtered_data.index.dayofyear,
                               filtered_data[col], label=str(year))
                    gx[i].set_title(self._labels[col])
                    gx[i].legend()

        data = self._data.copy()
        data["year"] = data.index.year
        sns.pairplot(data[["year", "pt01_succao", "lt01_nivel"]], hue="year")

        plt.legend()
        plt.tight_layout()
        plt.show()

    @staticmethod
    def _simplify_column_name(text):
        len_id = len("BAIRRO ALTO - ")

        if text != "Timestamp":
            text = text[len_id:]

        text = text.lower().replace("/", "").strip()
        text = re.sub("\s+", "_", text)
        text = unidecode.unidecode(text)
        return text


if __name__ == "__main__":
    eda = EDA(DATASET_PATH,
              VARIABLES,
              resample_period="1d",
              labels=AXIS_LABELS)
